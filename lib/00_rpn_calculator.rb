class RPNCalculator

  def initialize
     @stack = []
  end

  def push(num)
     @stack << num
  end

  def value
    @stack.last
  end

  def plus
    performer(:+)
  end

  def minus
    performer(:-)
  end

  def times
      performer(:*)
  end

  def divide
    performer(:/)
  end

  private
  def performer(operater)
    raise "calculator is empty" if @stack.size < 2
    first_number = @stack.pop
    second_number = @stack.pop
    case operater
    when :+
      @stack << first_number + second_number
    when :-
      @stack << second_number - first_number
    when :/
      @stack << second_number.fdiv(first_number)
    when :*
      @stack << second_number * first_number
    else
      raise "#{operater} is not a valid math operation"
      @stack << second_number
      @stack << first_number
    end
  end
  public
  def tokens(string)
      tokens_array = string.split(" ")
      tokens_array.map do | ele |
        [:/, :*, :+, :-].include?(ele.to_sym) ? ele.to_sym : Integer(ele)
      end
  end

    def evaluate(string)
       tokens = tokens(string)
       tokens.each do |token|
          case token
          when Integer
            push(token)
          else
            performer(token)
          end
       end
       value
    end

end
